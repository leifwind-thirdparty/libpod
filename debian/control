Source: libpod
Section: admin
Priority: optional
Standards-Version: 4.4.1
Maintainer: Dmitry Smirnov <onlyjob@debian.org>
Uploaders: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Build-Depends: debhelper-compat (= 12)
    ,bash-completion
    ,dh-golang
    ,go-md2man
    ,golang-any
    ,golang-dbus-dev (>= 5.0.2~)
    ,golang-ginkgo-dev
    ,golang-github-appc-cni-dev (>= 0.7.1~)
      ,golang-github-containernetworking-plugins-dev
    ,golang-github-buger-goterm-dev
    ,golang-github-containerd-cgroups-dev
    ,golang-github-containers-buildah-dev (>= 1.11.6~)
    ,golang-github-containers-image-dev (>= 5.0.0~)
    ,golang-github-containers-psgo-dev
    ,golang-github-containers-storage-dev (>= 1.12.13~)
    ,golang-github-coreos-bbolt-dev (>= 1.3.3~)
    ,golang-github-coreos-go-iptables-dev (>= 0.4.2~)
    ,golang-github-coreos-go-systemd-dev (>= 20~)
    ,golang-github-cyphar-filepath-securejoin-dev (>= 0.2.2~)
    ,golang-github-docker-distribution-dev (>= 2.7.1~)
    ,golang-github-docker-docker-dev (>= 19.03.4~)
    ,golang-github-docker-go-connections-dev (>= 0.4.0~)
    ,golang-github-docker-go-units-dev (>= 0.4.0~)
    ,golang-github-docker-spdystream-dev
    ,golang-github-ghodss-yaml-dev
    ,golang-github-google-shlex-dev
    ,golang-github-hashicorp-go-multierror-dev
    ,golang-github-influxdata-tail-dev (>= 1.0.0+git20180327.c434825-2~) | golang-github-hpcloud-tail-dev
    ,golang-github-imdario-mergo-dev
    ,golang-github-ishidawataru-sctp-dev
    ,golang-github-json-iterator-go-dev
    ,golang-github-klauspost-compress-dev (>= 1.7.2~)
    ,golang-github-klauspost-pgzip-dev
    ,golang-github-mailru-easyjson-dev
    ,golang-github-mrunalp-fileutils-dev
    ,golang-github-opencontainers-go-digest-dev
    ,golang-github-opencontainers-image-spec-dev
    ,golang-github-opencontainers-runc-dev (>= 1.0.0~rc9~)
    ,golang-github-opencontainers-runtime-tools-dev (>= 0.9.0~)
    ,golang-github-opencontainers-selinux-dev (>= 1.2.2~)
    ,golang-github-openshift-api-dev
    ,golang-github-openshift-imagebuilder-dev
    ,golang-github-pkg-errors-dev
    ,golang-github-pkg-profile-dev
    ,golang-github-pquerna-ffjson-dev
    ,golang-github-proglottis-gpgme-dev
    ,golang-github-safchain-ethtool-dev
    ,golang-github-seccomp-containers-golang-dev
    ,golang-github-sirupsen-logrus-dev
    ,golang-github-stretchr-testify-dev
    ,golang-github-ulikunitz-xz-dev
    ,golang-github-urfave-cli-dev
    ,golang-github-vbatts-tar-split-dev
    ,golang-github-vbauerster-mpb-dev (>= 3.4.0~)
    ,golang-github-varlink-go-dev
    ,golang-github-vishvananda-netlink-dev (>= 1.0.0+git20181030~)
    ,golang-github-vividcortex-ewma-dev
    ,golang-golang-x-crypto-dev
    ,golang-golang-x-sys-dev
    ,golang-gomega-dev
    ,golang-google-genproto-dev
    ,golang-google-grpc-dev
    ,golang-gopkg-inf.v0-dev
    ,golang-gopkg-tomb.v1-dev
    ,golang-go-zfs-dev
    ,golang-toml-dev
    ,golang-x-text-dev
    ,libapparmor-dev
    ,libbtrfs-dev
    ,libglib2.0-dev
    ,libostree-dev
    ,libdevmapper-dev
    ,varlink-go
Homepage: https://github.com/containers/libpod
Vcs-Browser: https://salsa.debian.org/debian/libpod
Vcs-Git: https://salsa.debian.org/debian/libpod.git
XS-Go-Import-Path: github.com/containers/libpod
Testsuite: autopkgtest-pkg-go

Package: podman
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${misc:Depends}, ${shlibs:Depends}
    ,conmon (>= 2.0.0~)
    ,containernetworking-plugins
    ,crun | runc (>= 1.0.0~rc9~)
Conflicts: slirp4netns (<< 0.4.1), fuse-overlayfs (<< 0.7.1)
Breaks: buildah (<< 1.10.1-6)
Recommends: ${misc:Recommends}
    ,buildah (>= 1.10.1-6~)
    ,fuse-overlayfs (>= 0.7.1~)
    ,slirp4netns (>= 0.4.1~)
    ,tini | dumb-init
    ,uidmap
Suggests: ${misc:Suggests}
    ,containers-storage
Description: engine to run OCI-based containers in Pods
 Podman is an engine for running OCI-based containers in Pods.
 Podman provides a CLI interface for managing Pods, Containers, and
 Container Images.
 .
 At a high level, the scope of libpod and podman is the following:
  • Support multiple image formats including the OCI and Docker image
    formats.
  • Support for multiple means to download images including trust & image
    verification.
  • Container image management (managing image layers, overlay filesystems,
    etc).
  • Full management of container lifecycle.
  • Support for pods to manage groups of containers together.
  • Resource isolation of containers and pods.
  • Support for a Docker-compatible CLI interface through Podman.
 .
 Podman is a daemon-less alternative to Docker.
